#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da RockLov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "isabella@gmail.com" e "Abc123"
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentar logar

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input        | senha_input | mensagem_output                  |
            | isabella@gmail.com | abc111      | Usuário e/ou senha inválidos.    |
            | isa@gmail.com      | Abc123      | Usuário e/ou senha inválidos.    |
            | isabella&gmail.com | Abc123      | Oops. Informe um email válido!   |
            |                    | Abc123      | Oops. Informe um email válido!   |
            | isabella@gmail.com |             | Oops. Informe sua senha secreta! |